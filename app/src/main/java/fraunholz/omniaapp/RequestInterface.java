package fraunholz.omniaapp;

import fraunholz.omniaapp.models.ServerRequest;
import fraunholz.omniaapp.models.ServerResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RequestInterface {

    @POST("testserver/")
    Call<ServerResponse> operation(@Body ServerRequest request);

}
