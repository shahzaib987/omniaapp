package fraunholz.omniaapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    private SharedPreferences pref;

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment enableFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    //mTextMessage.setText(R.string.title_profile);
                    //enableFragment = ProfileFragment.newInstance();
                    //initFragment();
                    if(pref.getBoolean(Constants.IS_LOGGED_IN,false)){
                        enableFragment = ProfileFragment.newInstance();
                    }else {
                        enableFragment = LoginFragment.newInstance();
                    }
                    break;
                    //return true;
                case R.id.navigation_contacts:
                    //mTextMessage.setText(R.string.title_contacts);
                    enableFragment = ContactsFragment.newInstance();
                    break;
                    //return true;
                case R.id.navigation_alarm:
                   // mTextMessage.setText(R.string.title_alarm);
                    enableFragment = MapsCurrentPlace.newInstance();
                    break;
                    //return true;
                case R.id.navigation_care:
                    //mTextMessage.setText(R.string.title_care);
                    enableFragment = CareFragment.newInstance();
                    break;
                    //return true;
                case R.id.navigation_settings:
                   // mTextMessage.setText(R.string.title_settings);
                    enableFragment = SettingsFragment.newInstance();
                    break;
                    //return true;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_frame, enableFragment);
            transaction.commit();
            return true;
            //return false;
        }


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getPreferences(0);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //Manually displaying the first fragment - one time only
       /* FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_frame, ProfileFragment.newInstance());
        transaction.commit();*/
       initFragment();
    }

    private void initFragment(){
        Fragment fragment;
        if(pref.getBoolean(Constants.IS_LOGGED_IN,false)){
            fragment = new ProfileFragment();
        }else {
            fragment = new LoginFragment();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,fragment);
        ft.commit();
    }

}


